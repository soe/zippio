Getting Started
===============

Documentation for this app is available at https://bit.ly/zippio-doc

Clone this repo:

    git clone https://soe@bitbucket.org/soe/zippio.git
    
Now cd into the directory:

    cd zippio
    
Install required gems:

    sudo bundle install

Now create a heroku app for it (this assumes you're already [logged into heroku](http://devcenter.heroku.com/articles/quickstart)):

    heroku create --stack cedar <project_name>
    
Ok, now we're ready to run on Heroku, but first we need to add 3 Heroku Add-ons:
Redis To Go is used for storage of package info and status.
IRON.io Worker is used for processing background workers effectively and cheaply.
PUBNUB is used for realtime messaging bet

    heroku addons:add redistogo
    heroku addons:add iron_worker:starter
    heroku addons:add pubnub:minimal
    
Then just push to heroku! 

    git push heroku master

Then go to http://<project_name>.herokuapp.com

Please note that you would need to set Heroku config.
First list out which config values are already there.

	heroku config

Then add the required ones:

	heroku config:add SFDC_USERNAME=##### SFDC_PASSWORD=##### SFDC_SECURITY_TOKEN=##### SFDC_URI=#####
	heroku config:add GDRIVE_USERNAME=##### GDRIVE_PASSWORD=#####
	heroku config:add REDISTOGO_URL=#####
	heroku config:add PUBNUB_PUBLISH_KEY=##### PUBNUB_SUBSCRIBE_KEY=##### PUBNUB_SECRET_KEY=#####
	heroku config:add IRON_WORKER_TOKEN=##### IRON_WORKER_PROJECT=#####

Notes for Salesforce account:

* Please enable all features, so that packages requiring these features would be installed smoothly by the install worker.

* Please whitelist all IP addresses for "workers" to login from remote virtual machines. To do that, please refer to this comprehensive guide: http://raydehler.com/cloud/clod/enable-all-trusted-ip-ranges-for-a-salesforce-org.html  
  
Live Demo
=========

Here's a live running demo of this app: http://bit.ly/zippio-demo

Development
===========

To run in development, you need to set config values in ".env" file. ".env-sample" is included. Edit the file to update the values there and rename the file to ".env".

    rackup -p 3000 config.ru

Project Structure
=================

This app is a simple Sinatra app with Ruby 1.9. 
To convert this app to terminal app, look at the two worker files.

"workers" folder contains .rb files which do background logics via IRON.io Worker.
"views" folder contains .erb files for templating.

Redis To Go is used for storage of package info and status.
IRON.io Worker is used for processing background workers effectively and cheaply.
PUBNUB is used for realtime messaging between background workers and user-clients (for smooth UX).
Google Drive is used for storage of package zip files.