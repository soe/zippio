require 'yaml'
require 'iron_worker'
require 'sinatra'
require 'metaforce'
require 'pubnub'
require 'redis'

# configure IronWorker
IronWorker.configure do |config|
  config.token = ENV['IRON_WORKER_TOKEN']
  config.project_id = ENV['IRON_WORKER_PROJECT_ID']
end

# call the Sinatra app - "app.rb"
require './app'
run Sinatra::Application
