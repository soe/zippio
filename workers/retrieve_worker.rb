
class RetrieveWorker < IronWorker::Base

  merge_gem 'metaforce'
  merge_gem 'highline'
  merge_gem 'google_drive'
  merge_gem 'pubnub'
  merge_gem 'redis'

  attr_accessor :queue_name, :token, :project_id, :package_id, :package_name, :config

  def run

    IronWorker.logger.info "package id: #{package_id}"
    IronWorker.logger.info "package name: #{package_name}"

    # initiate Pubsub
    pubnub = Pubnub.new(config['PUBNUB_PUBLISH_KEY'], config['PUBNUB_SUBSCRIBE_KEY'], config['PUBNUB_SECRET_KEY'], '', false)

    # initiate Redis
    uri = URI.parse(config["REDISTOGO_URL"])
    redis = Redis.new(:host => uri.host, :port => uri.port, :password => uri.password)

    # status update
    pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
      'message' => {'type' => 'status','text' => 'Starting retrieval...'}
    })

    client = Metaforce::Metadata::Client.new(
      :username => config["SFDC_USERNAME"],
      :password => config["SFDC_PASSWORD"],
      :security_token => config["SFDC_SECURITY_TOKEN"]
    )

    # progress update
    pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
      'message' => {'type' => 'progress','percent' => '20','text' => 'Logging in... 20%'}
    })

    # Salesforce metadata retrieve - get zip of the package by package name
    result = client.retrieve({:options => {
      :package_names => package_name, 
      :single_package => true
    }})

    # progress update
    pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
      'message' => {'type' => 'progress','percent' => '50','text' => 'Retrieving... 50%'}
    })

    # initiate GoogleDrive
    drive = GoogleDrive.login(config["GDRIVE_USERNAME"], config["GDRIVE_PASSWORD"])

    # progress update
    pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
      'message' => {'type' => 'progress','percent' => '70','text' => 'Connecting to Google Drive... 70%'}
    })

    # delete old files with same name - search by title
    files = drive.files("title" => "sf-zippio-#{package_id}.zip", "title-exact" => true)

    # loop through files and delete them permanently
    for f in files
      f.delete(true)
    end # for

    # create file
    drive_file = drive.upload_from_string(result.zip_file, "sf-zippio-#{package_id}.zip", :convert => false)

    # progress update
    pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
      'message' => {'type' => 'progress','percent' => '100','text' => 'Upload to Google Drive... 100%'}
    })

    # status update
    pubnub.publish({'channel' => package_id,'callback' => lambda {|x| x},
      'message' => {'type' => 'status','text' => 'Retrieval complete...'}
    })

    # if save to google drive successful
    if drive_file
      # update the package's status and file
      redis.set "p:#{package_id}:status", "retrieved"
      redis.set "p:#{package_id}:file", drive_file.resource_id
    end
  end # def:run
end # class:RetrieveWorker